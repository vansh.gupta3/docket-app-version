export default {
    gradientLogin:"rgba(132, 155, 218, 0.5)",
    gradient2Login:'rgba(77, 98, 179, 0.7)',
    fontColor:'#232324',
    btnColor:'#FC5A5A',
    primaryDashboard:'rgba(82, 100, 204, 1)',
    primaryDashboard2:'rgba(105, 123, 228, 1)'
}