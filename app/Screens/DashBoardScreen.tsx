import {
  View,
  Text,
  StyleSheet,
  Button,
  SafeAreaView,
  Platform,
  StatusBar,
} from "react-native";
import React from "react";
import { signOut } from "firebase/auth";
import { auth } from "../Backend";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { Search, Bell } from "../assets";
import { Image } from "expo-image";
import colors from "../colors";
import { LinearGradient } from "expo-linear-gradient";
import { useFonts } from "expo-font";
import { Roboto_700Bold } from "@expo-google-fonts/roboto";
import { Link } from "expo-router";
export const DashBoardScreen = () => {
  const [fontsLoaded, fontError] = useFonts({
    Roboto_700Bold,
  });

  if (!fontsLoaded && !fontError) {
    return null;
  }
  return (
        <View style={styles.outerContainer}>
      <LinearGradient
        colors={[colors.primaryDashboard, colors.primaryDashboard2]}
        style={styles.navbarGradient}
      >
        <View style={styles.textContainer}>
          <Text style={styles.text}>Dashboard</Text>
        </View>

        <View style={styles.images}>
          <Image source={Search} style={styles.logo} />
          <Image source={Bell} style={styles.logo} />
        </View>
      </LinearGradient>
      <Link href='index'>HomeScreen</Link>
      <View style={styles.filter}></View>
      <View style={styles.tasks}></View>
      <Button
        title="Sign Out"
        onPress={async () => {
          await signOut(auth);
          AsyncStorage.clear();
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  textContainer: {
    flex: 2,
    justifyContent:'space-around',
    flexDirection:'row',
  },
  text: {
    fontFamily: "Roboto_700Bold",
    fontSize: 27,
    color: "white",
  },
  images: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-evenly",
  },
  filter: {
    flex: 1,
  },
  logo: {
    width: 30,
    height: 30,
  },
  navbarGradient: {
    flex: 1,
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "center",
    paddingBottom: 16,
    borderBottomRightRadius: 25,
    borderBottomLeftRadius: 25,
  },
  outerContainer: {
    flex: 1,
  },
  tasks: {
    flex: 5,
  },
});

